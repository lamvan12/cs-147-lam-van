﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterMovement : MonoBehaviour {

    public float speed;
    private Rigidbody rb;
    private Animator animator;
    public GameObject bulletPrefab;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // make bullet
            GameObject newBullet = GameObject.Instantiate(bulletPrefab);

            // position
            newBullet.transform.position = 
                gameObject.transform.position + new Vector3(0, 0.1f, 0);

            //force
            Rigidbody bulletBody = newBullet.GetComponent<Rigidbody>();
            bulletBody.AddForce(gameObject.transform.forward * speed, ForceMode.Impulse);

        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            Vector3 movement = new Vector3(0, .1f, 0);
            rb.AddForce(movement * speed, ForceMode.Impulse);
        }

    }
	void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

        if(movement.sqrMagnitude > 0)
            animator.SetTrigger("BoomTrigger");// .SetBool("BOOM", movement.sqrMagnitude > 0);
    }
}
