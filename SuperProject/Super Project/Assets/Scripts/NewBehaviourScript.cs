﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {
    public float speed;
    private Rigidbody rb;
    public GameObject bulletPrefab;


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>(); 	
	}

    //Add bullet-firing to the ball
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // make bullet
            GameObject newBullet = GameObject.Instantiate(bulletPrefab);

            // position
            newBullet.transform.position =
                gameObject.transform.position + new Vector3(0, 0, 1f);

            //force
            Rigidbody bulletBody = newBullet.GetComponent<Rigidbody>();
            bulletBody.AddForce(gameObject.transform.forward * speed, ForceMode.Impulse);
        }
        //Use key b to jump the main object up
        if (Input.GetKeyDown(KeyCode.J))
        {
            Vector3 movement = new Vector3(0, 1f, 0);
            rb.AddForce(movement * speed, ForceMode.Impulse);
        }
    }
    // Movement cotrollers
    void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }
}
